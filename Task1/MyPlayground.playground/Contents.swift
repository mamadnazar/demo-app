import UIKit

// EXERCISE #1
print("Convert negative numbers in an array into zero")
let numbers: [Int] = [1, -1, 0, 5, -100, 5, 2, -3, -9, -54, 2, 200]
print("Initial array: \(numbers)")

// 1st way
var updatedNumbers: [Int] = numbers.map { num in
    if num < 0 {
        return 0
    }
    return num
}

// 2nd way
// var updatedNumbers = numbers.map { $0 < 0 ? 0 : $0}

print("Updated array: \(updatedNumbers)")
print()

// EXERCISE #2
print("Sort an array in an ascending order")
print("Initial array: \(numbers)")
updatedNumbers = numbers.sorted()
print("Updated array: \(updatedNumbers)")
print()

print("Sort an array in a descending order")
print("Initial array: \(numbers)")
updatedNumbers = numbers.sorted(by: {$0 > $1})
print("Updated array: \(updatedNumbers)")
print()

// EXERCISE #3
print("Min value in an array: \(numbers.min()!)")
print("Min value in an array: \(numbers.max()!)")
print()

// EXERCISE #4
print(numbers)
let sum = numbers.reduce(0, {sum, number in sum + number})
print("Sum of all values of the above array: \(sum)")
