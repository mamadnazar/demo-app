print("Convert negative numbers in an array into zero:")
let numbers: [Int] = [1, -1, 0, 5, -100, 5, 2, -3, -9, -54, 2, 200]
print("Initial array: \(numbers)")

// 1st way
//var updatedNumbers: [Int] = numbers.map { num in
//    if num < 0 {
//        return 0
//    }
//    return num
//}

// 2nd way
 let updatedNumbers = numbers.map { $0 < 0 ? 0 : $0}

print("Updated array: \(updatedNumbers)")
