print("Sort an array in an ascending order")
var numbers: [Int] = [1, -1, 0, 5, -100, 5, 2, -3, -9, -54, 2, 200]
print("Initial array: \(numbers)")
numbers.sort()
print("Updated array: \(numbers)")
print()

print("Sort an array in a descending order")
print("Initial array: \(numbers)")
numbers.sort(by: {$0 > $1})
print("Updated array: \(numbers)")
