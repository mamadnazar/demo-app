let numbers: [Int] = [1, -1, 0, 5, -100, 5, 2, -3, -9, -54, 2, 200]
print("Initial array: \(numbers)")

let sum = numbers.reduce(0, {sum, number in sum + number})
print("Sum of all values of the above array: \(sum)")
