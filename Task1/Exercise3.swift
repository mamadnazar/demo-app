print("Finding min and max values of an array")
let numbers: [Int] = [1, -1, 0, 5, -100, 5, 2, -3, -9, -54, 2, 200]
print("Initial array: \(numbers)")

print("Min value in an array: \(numbers.min()!)")
print("Min value in an array: \(numbers.max()!)")
