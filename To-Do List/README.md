# TO-DO List App

Simple TODO List Demo App using Realm Database

## How to Login?

Use below credentials to log in as an `Admin`:

```
Username : morpheus
Password : bluepillsucks
```

Other users are `Developers`:

```
Username : aliceinwww
Password : morpheusgavemethewrongpill
```

```
Username : vegandexter
Password : iamnotveganlol
```

```
Username : noobmaster69
Password : cucumber1
```

```
Username : joker
Password : goldonmywrist
```

Admin priviliges are:
  - Create New Task
  - Edit Task
  - Assign Developer to Task
  - Assign Deadline
  - Delete Task
  - Close Task
  - Write Comments

Developer priviliges are:
  - Change state of a Task
  - Write Comments

## Note

No notifications feature due to not having paid apple developer account :(


