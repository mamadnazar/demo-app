//
//  LoginViewController.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/23/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: RoundedButton!
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLoginButton()
    }

    private func setupLoginButton() {
        if loginTextField.text == "" || passwordTextField.text == "" {
            setupChangedButton(isEnabled: false, button: loginButton)
        }
    }
    
    @IBAction func loginButtonClicked(_ sender: RoundedButton) {
        login(button: sender)
    }
    
    private func login(button: RoundedButton) {
        let username = "\(loginTextField.text ?? "")"
        let password = "\(passwordTextField.text ?? "")"
        if (username == "" || password == "") {
            showErrorAlert(message: "Please enter all the fields")
        } else {
            guard let user = realm.objects(User.self).filter( {$0.getUsername() == username} ).first else {
                showErrorAlert(message: "Invalid username")
                return
            }
            if user.getPassword() != password {
                showErrorAlert(message: "Invalid password")
                return
            }
            DataManager.shared.setUsername(username: user.getUsername())
            presentMainVC()
        }
    }
    
    private func presentMainVC() {
        let sb = UIStoryboard(name: StoryboardIdentifier.main, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.mainViewController)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setupChangedButton(isEnabled: true, button: loginButton)
    }
}
