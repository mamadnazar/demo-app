//
//  User.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/23/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import Foundation
import RealmSwift

enum Role: String {
    case admin = "Admin"
    case developer = "Developer"
}

class User: Object {
    @objc private dynamic var username: String = ""
    @objc private dynamic var password: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var role: String = ""
    var tasks = List<Task>()
    
    required init() {}
    
    init(username: String, password: String, firstName: String, lastName: String, role: String) {
        self.username = username
        self.password = password
        self.firstName = firstName
        self.lastName = lastName
        self.role = role
    }
    
    public func getUsername() -> String {
        return username
    }
    
    public func getPassword() -> String {
        return password
    }
    
    public func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
    
    public func getUserRole() -> String {
        return role
    }
    
}

