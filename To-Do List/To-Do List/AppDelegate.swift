//
//  AppDelegate.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/21/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let realm = try! Realm()
    private var rootVC: UIViewController?
    private var sb: UIStoryboard?
    var navigationController: UINavigationController = UINavigationController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        print(Realm.Configuration.defaultConfiguration.fileURL)
        if (UserDefaults.standard.bool(forKey: "First Launch") == false) {
            setupData()
        }
        UserDefaults.standard.set(true, forKey: "First Launch")
        
        if (DataManager.shared.getUsername() != "") {
            sb = UIStoryboard(name: StoryboardIdentifier.main, bundle: nil)
            rootVC = sb?.instantiateViewController(withIdentifier: ViewControllerIdentifier.mainViewController) as! MainViewController
        } else {
            sb = UIStoryboard(name: StoryboardIdentifier.login, bundle: nil)
            rootVC = sb?.instantiateViewController(withIdentifier: ViewControllerIdentifier.loginViewController) as! LoginViewController
        }
        
        navigationController.viewControllers = [rootVC] as! [UIViewController]
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    private func setupData() {
        
        let comment1 = Comment(id: 1, senderName: "John Doe", date: "20/10/2020", message: "Hello")
        let comment2 = Comment(id: 2, senderName: "Bakytbel", date: "20/10/1021", message: "Eu Salamalaikum EEE")
        let comment3 = Comment(id: 3, senderName: "LolYch", date: "10/09/1923", message: "WTF? WHo named me like that")
        let comment4 = Comment(id: 4, senderName: "uWu", date: "30/02/2000", message: "uWu")
        let comment5 = Comment(id: 5, senderName: "InstaTelo4ka", date: "01/01/2001", message: "i 4o 4to ya pishu 4erez 4")
        let comment6 = Comment(id: 6, senderName: "Emhyr", date: "01/01/1313", message: "Ciri Wasssuuuuuup")
        let comment7 = Comment(id: 7, senderName: "Geralt", date: "02/03/1312", message: "Hmmmmm")
        let comment8 = Comment(id: 8, senderName: "Deidara", date: "02/03/1909", message: "Vzryyyyv!")
        let comment9 = Comment(id: 9, senderName: "Luntik", date: "03/06/1414", message: "Ya Rodilsya")
        let comment10 = Comment(id: 10, senderName: "Dim Dimich", date: "13/10/2019", message: "A kto takie fiksiki?")
        
        let task1 = Task(id: 1, title: "Learn MVVM", state: State.in_progress.rawValue, deadline: "21/05/2020")
        let task2 = Task(id: 2, title: "Fix the bug", state: State.in_progress.rawValue, deadline: "10/10/2010")
        let task3 = Task(id: 3, title: "Cycling", state: State.not_started.rawValue, deadline: "01/09/2020")
        let task4 = Task(id: 4, title: "Reading fiction", state: State.in_progress.rawValue, deadline: "23/10/2019")
        let task5 = Task(id: 5, title: "Fix phone camera", state: State.completed.rawValue, deadline: "01/01/2001")
        let task6 = Task(id: 6, title: "Workout", state: State.completed.rawValue, deadline: "01/01/2001")
        
        task1.comments.append(comment1)
        task1.comments.append(comment2)
        
        task2.comments.append(comment3)
        task2.comments.append(comment4)
        
        task3.comments.append(comment5)
        
        task4.comments.append(comment6)
        task4.comments.append(comment7)
        task4.comments.append(comment8)
        task4.comments.append(comment9)
        
        task5.comments.append(comment10)
        
        let user1 = User(username: "morpheus", password: "bluepillsucks", firstName: "Morpheus", lastName: "Wachowski", role: Role.admin.rawValue)
        let user2 = User(username: "noobmaster69", password: "cucumber1", firstName: "Geralt", lastName: "Rivec", role: Role.developer.rawValue)
        let user3 = User(username: "aliceinwww", password: "morpheusgavemethewrongpill", firstName: "Alice", lastName: "Wonderland", role: Role.developer.rawValue)
        let user4 = User(username: "vegandexter", password: "iamnotveganlol", firstName: "Dexter", lastName: "Hannibal", role: Role.developer.rawValue)
        let user5 = User(username: "joker", password: "goldonmywrist", firstName: "John", lastName: "Doe", role: Role.developer.rawValue)
        
        user1.tasks.append(task1)
        user1.tasks.append(task2)
        user1.tasks.append(task3)
        user1.tasks.append(task4)
        user1.tasks.append(task5)
        user1.tasks.append(task6)
        
        user2.tasks.append(task1)
        task1.assignedPerson = user2
        
        user3.tasks.append(task2)
        user3.tasks.append(task3)
        task2.assignedPerson = user3
        task3.assignedPerson = user3
        
        user4.tasks.append(task4)
        user4.tasks.append(task6)
        task4.assignedPerson = user4
        task6.assignedPerson = user4
        
        user5.tasks.append(task5)
        task5.assignedPerson = user5
        
        try! realm.write {
            realm.add(user1)
            realm.add(user2)
            realm.add(user3)
            realm.add(user4)
            realm.add(user5)
        }
    }

}

