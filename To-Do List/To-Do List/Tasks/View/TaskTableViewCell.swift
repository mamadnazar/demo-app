//
//  TasksTableViewCell.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/21/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskTitleLabel: UILabel!
    
    func setTaskCell(task: Task) {
        taskTitleLabel.text = task.title
        if task.isClosed == 1 {
            taskTitleLabel.textColor = Color.green
        } else {
            taskTitleLabel.textColor = UIColor.black
        }
    }
    
}
