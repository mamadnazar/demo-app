//
//  Task.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/21/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import Foundation
import RealmSwift

enum State: String {
    case not_started = "Not Started"
    case in_progress = "In Progress"
    case completed = "Completed"
}

class Task: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var state: String = State.not_started.rawValue
    @objc dynamic var assignedPerson:User?
    @objc dynamic var deadline = "20/20/2020"
    @objc dynamic var isClosed = 0
    var comments = List<Comment>()
    
    required init() {}
    
    init(id: Int, title: String, state: String, deadline: String) {
        self.id = id
        self.title = title
        self.state = state
        self.deadline = deadline
        self.isClosed = 0
    }
}
