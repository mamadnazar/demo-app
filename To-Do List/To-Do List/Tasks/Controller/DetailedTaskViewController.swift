//
//  DetailedTaskViewController.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class DetailedTaskViewController: UIViewController {

    @IBOutlet weak var taskTitleTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var assignedPersonTextField: UITextField!
    @IBOutlet weak var deadlineTextField: UITextField!
    @IBOutlet weak var closeTaskButton: GreenButton!
    @IBOutlet weak var commentsButton: CardButton!
    
    var isAddingNewTask: Bool = false
    private let pickerView = UIPickerView()
    private let datePickerView = UIDatePicker()
    private var currentTextField = UITextField()
    private var user = User()
    
    private var task = Task()
    let realm = try! Realm()
    
    private let states = [State.not_started, State.in_progress, State.completed]
    private var developers: [User] = []
    private var assignedUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        user = realm.objects(User.self).filter({$0.getUsername() == DataManager.shared.getUsername()}).first ?? User()
        setupNavigationBar()
        customizeVC()
        instantiatePickerView()
    }
    
    @IBAction func closeTask(_ sender: Any) {
            let rlmTasks = realm.objects(Task.self).filter("id == \(task.id)")
            if let rlmTask = rlmTasks.first {
            try! realm.write {
                rlmTask.isClosed = 1
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func openComments(_ sender: Any) {
        let sb = UIStoryboard(name: StoryboardIdentifier.comments, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.commentsViewController) as! CommentsViewController
        vc.task = self.task
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func customizeVC() {
        developers = Array(realm.objects(User.self).filter({$0.getUserRole() == Role.developer.rawValue}))
        assignedUser = task.assignedPerson ?? User()
        
        if user.role == Role.developer.rawValue {
            closeTaskButton.isHidden = true
            taskTitleTextField.isEnabled = false
            stateTextField.isEnabled = true
            assignedPersonTextField.isEnabled = false
            deadlineTextField.isEnabled = false
            fetchData()
        } else {
            stateTextField.isEnabled = false
            if isAddingNewTask {
                commentsButton.isHidden = true
                closeTaskButton.isHidden = true
            } else {
                commentsButton.isHidden = false
                closeTaskButton.isHidden = false
                fetchData()
            }
        }
        
        if task.isClosed == 1 {
            closeTaskButton.isHidden = true
            taskTitleTextField.isEnabled = false
            stateTextField.isEnabled = false
            assignedPersonTextField.isEnabled = false
            deadlineTextField.isEnabled = false
            fetchData()
        }
        
    }
    
    private func fetchData() {
        taskTitleTextField.text = task.title
        stateTextField.text = task.state
        assignedPersonTextField.text = task.assignedPerson?.getFullName()
        deadlineTextField.text = task.deadline
    }
    
    func setTask(task: Task) {
        self.task = task
    }
    
    private func setupNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didEditTask))
    }
    
    @objc private func didEditTask() {
        
        fillTaskData()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func fillTaskData() {
        let title = (taskTitleTextField.text ?? "")
        let state = (stateTextField.text ?? "")
        let assignedPerson = (assignedPersonTextField.text ?? "")
        let deadline = (deadlineTextField.text ?? "")
    
        if title.isEmpty || state.isEmpty || assignedPerson.isEmpty || deadline.isEmpty {
            showErrorAlert(message: "Please fill all of the data fields to proceed")
            return
        }
        
        if isAddingNewTask {
            addNewTask(title: title, state: state, assignedPerson: assignedPerson, deadline: deadline)
        } else {
            updateTask(title: title, state: state, assignedPerson: assignedPerson, deadline: deadline)
        }
    }
    
    private func addNewTask(title: String, state: String, assignedPerson: String, deadline: String) {
        let newTask = Task()
        newTask.id = (realm.objects(Task.self).max(ofProperty: "id") ?? 0)+1
        newTask.title = title
        newTask.state = state
        newTask.deadline = deadline
        newTask.assignedPerson = assignedUser
        
        try! realm.write {
            realm.add(newTask)
            // Add new task to admin
            realm.objects(User.self).filter({$0.getUserRole() == Role.admin.rawValue}).first?.tasks.append(newTask)
            // Add new task to assigned user
            realm.objects(User.self).filter({$0.getUsername() == self.assignedUser.getUsername()}).first?.tasks.append(newTask)
        }
    }
    
    private func updateTask(title: String, state: String, assignedPerson: String, deadline: String) {
        let rlmTask = realm.objects(Task.self).filter("id == \(task.id)").first
        let prevUser = rlmTask?.assignedPerson
        try! realm.write {
            rlmTask?.title = title
            rlmTask?.state = state
            rlmTask?.assignedPerson = assignedUser
            rlmTask?.deadline = deadline
            // Add this task to new assigned user
            realm.objects(User.self).filter({$0.getUsername() == self.assignedUser.getUsername()}).first?.tasks.append(rlmTask!)
            // Remove this task from prev assigned user
            let prevUserTasks = realm.objects(User.self).filter({$0.getUsername() == prevUser?.getUsername()}).first?.tasks
            if let indexToRemoveAt = prevUserTasks?.index(of: rlmTask!) {
                prevUserTasks?.remove(at: indexToRemoveAt)
            }
        }
    }
    
}

extension DetailedTaskViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    private func instantiatePickerView() {
        pickerView.dataSource = self
        pickerView.delegate = self
        stateTextField.inputView = pickerView
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: .valueChanged)
        deadlineTextField.inputView = datePickerView
    }
     
    @objc private func datePickerValueChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.medium
        formatter.timeStyle = DateFormatter.Style.none
        deadlineTextField.text = formatter.string(from: sender.date)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currentTextField {
            case stateTextField: return states.count
            case assignedPersonTextField: return developers.count
            default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch currentTextField {
            case stateTextField: stateTextField.text = states[row].rawValue
            case assignedPersonTextField:
                assignedPersonTextField.text = developers[row].getFullName()
                assignedUser = developers[row]
            default: return
        }
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currentTextField {
            case stateTextField: return states[row].rawValue
            case assignedPersonTextField: return developers[row].getFullName()
            default: return "N/A"
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        currentTextField.inputView = pickerView
    }
    
}
