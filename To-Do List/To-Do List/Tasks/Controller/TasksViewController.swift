//
//  TasksViewController.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/21/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class TasksViewController: UIViewController {

    @IBOutlet weak var tasksTableView: UITableView!
    @IBOutlet weak var showAllTasksButton: CardButton!
    var tasks: [Task] = []
    let realm = try! Realm()
    private var user = User()
    private var shownAllTasks: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        user = realm.objects(User.self).filter({$0.getUsername() == DataManager.shared.getUsername()}).first ?? User()
        fetchData()
        tasksTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        user = realm.objects(User.self).filter({$0.getUsername() == DataManager.shared.getUsername()}).first ?? User()
        fetchData()
        setupNavigationBar()
    }
    
    private func fetchData() {
        let user = realm.objects(User.self).filter({$0.getUsername() == self.user.getUsername()}).first
        if shownAllTasks {
            self.tasks = Array(user?.tasks ?? List<Task>())
            showAllTasksButton.setTitle("Hide Closed Tasks", for: .normal)
        } else {
            if let filteredTasks = user?.tasks.filter("isClosed == 0") {
                self.tasks = Array(filteredTasks)
                showAllTasksButton.setTitle("Show Closed Tasks", for: .normal)
            }
        }
    }
    
    private func setupNavigationBar() {
        if (user.role == Role.admin.rawValue) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTask))
        }
    }

    @objc private func addTask() {
        let sb = UIStoryboard(name: StoryboardIdentifier.tasks, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.detailedTaskViewController) as! DetailedTaskViewController
        vc.isAddingNewTask = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showAllTasks(_ sender: Any) {
        let user = realm.objects(User.self).filter({$0.getUsername() == self.user.getUsername()}).first
        if shownAllTasks {
            self.tasks = Array(user?.tasks ?? List<Task>())
            showAllTasksButton.setTitle("Hide Closed Tasks", for: .normal)
        } else {
            if let filteredTasks = user?.tasks.filter("isClosed == 0") {
                self.tasks = Array(filteredTasks)
                showAllTasksButton.setTitle("Show Closed Tasks", for: .normal)
            }
        }
        self.shownAllTasks = !shownAllTasks
        tasksTableView.reloadData()
    }
    
    private func deleteFromRealm(at id: Int) {
        let rlmTasks = realm.objects(Task.self).filter("id == \(id)")
        //TODO: SHOULD I DELETE FROM USER"S AS WELL ????
        if let rlmTask = rlmTasks.first {
            try! realm.write {
                realm.delete(rlmTask)
            }
        }
    }
    
}

extension TasksViewController: UITableViewDataSource, UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.taskTableViewCell, for: indexPath) as! TaskTableViewCell
        cell.setTaskCell(task: tasks[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: StoryboardIdentifier.tasks, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.detailedTaskViewController) as! DetailedTaskViewController
        vc.setTask(task: tasks[indexPath.row])
        vc.isAddingNewTask = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (user.getUserRole() == Role.admin.rawValue) {
            if editingStyle == .delete {
                    deleteFromRealm(at: tasks[indexPath.row].id)
                    tasks.remove(at: indexPath.row)
                    tasksTableView.beginUpdates()
                    tasksTableView.deleteRows(at: [indexPath], with: .fade)
                    tasksTableView.endUpdates()
            }
        }
    }
}
