//
//  Comment.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import Foundation
import RealmSwift

class Comment: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var senderName: String = ""
    @objc dynamic var date: String = ""
    @objc dynamic var message: String = ""
    
    required init() {}
    
    init(id: Int, senderName: String, date: String, message: String) {
        self.id = id
        self.senderName = senderName
        self.date = date
        self.message = message
    }
    
}
