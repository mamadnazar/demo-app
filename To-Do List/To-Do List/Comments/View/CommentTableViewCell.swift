//
//  DetailedTaskTableViewCell.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var commentDate: UILabel!
    @IBOutlet weak var commentMessage: UILabel!
    
    func setCell(comment: Comment) {
        senderName.text = comment.senderName
        commentDate.text = comment.date
        commentMessage.text = comment.message
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
