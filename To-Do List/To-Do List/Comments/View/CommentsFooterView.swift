//
//  CommentsFooterView.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

class CommentsFooterView: UITableViewHeaderFooterView, UITextFieldDelegate {

    @IBOutlet weak var commentMessageTextField: UITextField!
    @IBOutlet weak var sendCommentButton: CardButton!
    var sendComment: ((_ message: String) -> ())?
    
    override func awakeFromNib() {
        commentMessageTextField.delegate = self
    }
    
    @IBAction func sendCommentPressed(_ sender: Any) {
        sendComment?(commentMessageTextField.text ?? "")
    }
}
