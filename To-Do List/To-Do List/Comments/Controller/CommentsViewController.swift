//
//  DetailedTaskViewController.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class CommentsViewController: UIViewController {
        
    
    @IBOutlet weak var commentsTableView: UITableView!
    var comments: [Comment] = []
    var task = Task()
    private var commentMessage = ""
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
        registerFooterView()
    }
    
    private func fetchData() {
        comments = Array(task.comments)
    }
    
    func sendComment(_ message: String) -> () {
        if !message.isEmpty {
            commentMessage = message
            let newComment = createNewComment()
            try! realm.write {
                realm.objects(Task.self).filter("id == \(task.id)").first?.comments.append(newComment)
            }
            fetchData()
            commentsTableView.reloadData()
        }
    }

    private func createNewComment() -> Comment {
        let newComment = Comment()
        newComment.id = (realm.objects(Comment.self).max(ofProperty: "id") ?? 0)+1
        newComment.message = commentMessage
        newComment.senderName = realm.objects(User.self).filter({$0.getUsername() == DataManager.shared.getUsername()}).first?.getFullName() ?? "N/A"
        //newComment.senderName = DataManager.shared.getUser().getFullName()
        newComment.date = self.getCurrentDate()
        return newComment
    }
}

extension CommentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    private func registerFooterView() {
        let footerNib = UINib.init(nibName: "CommentsFooterView", bundle: Bundle.main)
        commentsTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "CommentsFooterView")
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentsFooterView") as! CommentsFooterView
        footerView.sendComment = sendComment
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.commentTableViewCell, for: indexPath) as! CommentTableViewCell
        cell.setCell(comment: comments[indexPath.row])
        return cell
    }
    
}
