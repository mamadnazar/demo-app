//
//  Extensions.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showErrorAlert(message: String) {
        let alertController = UIAlertController(title: "Hey!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setupChangedButton(isEnabled: Bool, button: UIButton) {
        if isEnabled {
            button.backgroundColor = Color.turquoise
            button.isEnabled = true
        } else {
            button.backgroundColor = Color.gray
            button.isEnabled = false
        }
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
}

