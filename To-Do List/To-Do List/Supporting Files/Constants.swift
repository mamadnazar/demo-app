//
//  Constants.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

struct Color {
    static let lightGray  = UIColor(red: 173/255.0, green: 173/255.0, blue: 173/255.0, alpha: 1.0)
    static let turquoise  = UIColor(red: 56/255.0, green: 206/255.0, blue: 206/255.0, alpha: 1.0)
    static let black      = UIColor(red: 41/255.0, green: 41/255.0, blue: 41/255.0, alpha: 1.0)
    static let darkGray   = UIColor(red: 77/255.0, green: 77/255.0, blue: 77/255.0, alpha: 1.0)
    static let ivory      = UIColor(red: 232/255.0, green: 232/255.0, blue: 232/255.0, alpha: 1.0)
    static let gray       = UIColor(red: 126/255.0, green: 126/255.0, blue: 126/255.0, alpha: 1.0)
    static let lightBlue  = UIColor(red: 73/255.0, green: 175/255.0, blue: 249/255.0, alpha: 1.0)
    static let red        = UIColor(red: 255/255.0, green: 89/255.0, blue: 89/255.0, alpha: 1.0)
    static let green  = UIColor(red: 46/255.0, green: 139/255.0, blue: 87/255.0, alpha: 1.0)
}

struct StoryboardIdentifier {
    static let main = "Main"
    static let tasks = "Tasks"
    static let comments = "Comments"
    static let login = "Login"
}

struct ViewControllerIdentifier {
    static let mainViewController = "MainViewController"
    static let tasksViewController = "TasksViewController"
    static let detailedTaskViewController = "DetailedTaskViewController"
    static let commentsViewController = "CommentsViewController"
    static let loginViewController = "LoginViewController"
}

struct CellIdentifier {
    static let taskTableViewCell = "TaskTableViewCell"
    static let commentTableViewCell = "CommentTableViewCell"
}
