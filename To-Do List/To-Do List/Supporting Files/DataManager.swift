//
//  DataManager.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class DataManager {

    class var shared: DataManager {
        struct Static {
            static let instance = DataManager()
        }
        return Static.instance
    }

    private var userIsAuthorized = "userIsAuthorized"
    private var user = User()
    
    func getUsername() -> String {
        if let value = UserDefaults.standard.value(forKey: userIsAuthorized) as? String {
            return value
        }
        return ""
    }
    
    func setUsername(username: String) {
        UserDefaults.standard.set(username, forKey: userIsAuthorized)
    }
    
}

