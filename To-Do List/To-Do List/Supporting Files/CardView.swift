//
//  CardView.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/22/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit

class CardView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCardView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupCardView()
    }
    
    private func setupCardView() {
        backgroundColor = UIColor.white
        layer.cornerRadius = 7
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 4)
    }

}

class CardButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCardButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupCardButton()
    }
    
     private func setupCardButton() {
        backgroundColor = Color.turquoise
        layer.cornerRadius = 7
        setTitleColor(UIColor.white, for: .normal)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
}

class GreenButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCardButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupCardButton()
    }
    
     private func setupCardButton() {
        backgroundColor = Color.green
        setTitleColor(UIColor.white, for: .normal)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
}

class RoundedButton: UIButton {

private var activityIndicator: UIActivityIndicatorView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupRoundedButton()
        confgureIndicator()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupRoundedButton()
        confgureIndicator()
    }

    private func setupRoundedButton() {
        backgroundColor = Color.turquoise
        setTitleColor(.white, for: .normal)
        layer.cornerRadius = self.frame.size.height / 2
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowRadius = 2
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowOpacity = 0.6
    }

    private func confgureIndicator(){
        activityIndicator = UIActivityIndicatorView(frame:CGRect(origin: .zero, size: CGSize(width: 24, height: 24)))
        activityIndicator.style = .white
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        let xConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([xConstraint, yConstraint])
    }

    func stopIndicatorAnimation() {
        activityIndicator.stopAnimating()
        self.isEnabled = true
        setTitle(self.titleLabel?.text, for: .normal)
    }

    func startIndicatorAnimation() {
        activityIndicator.startAnimating()
        self.isEnabled = false
        setTitle("", for: .normal)
    }

}
