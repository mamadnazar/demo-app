//
//  ViewController.swift
//  To-Do List
//
//  Created by Shohkarim Mamadnazar on 5/21/20.
//  Copyright © 2020 Shohkarim Mamadnazar. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {

    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        customizeVC()
    
    }
    
    private func setupNavigationBar() {
        self.title = "Main Page"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(logout))
    }
    
    private func customizeVC() {
        let user = realm.objects(User.self).filter({$0.getUsername() == DataManager.shared.getUsername()}).first
        nameLable.text = user?.firstName
        surnameLabel.text = user?.lastName
        roleLabel.text = user?.role
    }
    
    @objc private func logout() {
        DataManager.shared.setUsername(username: "")
        let sb = UIStoryboard(name: StoryboardIdentifier.login, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.loginViewController) as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func openTasks(_ sender: Any) {
        let sb = UIStoryboard(name: StoryboardIdentifier.tasks, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: ViewControllerIdentifier.tasksViewController) as! TasksViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
